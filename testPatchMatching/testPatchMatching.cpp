#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/cudaarithm.hpp"
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <unordered_set>
#include "patchMatching\patch_matching.h"


using namespace cv;
using namespace std;
using namespace cv::cuda;

int main(int argc, char* argv[]){
	Mat src = imread(argv[1]);
	//resize(src, src, Size(src.cols/2, src.rows/2));
	Mat tar = imread(argv[2]);
	//resize(tar, tar, Size(tar.cols/2, tar.rows/2));
	/*int sx = 531/2, sy = 491/2, sw = 33/2, sh = 44/2;
	int tx = 512/2, ty = 484/2, tw = 33/2, th = 44/2;*/
	cout << src.rows << "\t" << src.cols << endl;
	cout << tar.rows << "\t" << tar.cols << endl;

	int sx = 247, sy = 115, sw = 19, sh = 46;
	int tx = 248, ty = 115, tw = 21, th = 46;

	PatchMatching pm;
	pm.set_src(src);
	pm.set_tar(tar);
	pm.set_src_patch(sx, sy, sw, sh);
	pm.set_tar_patch(tx, ty, tw, th);
	
	/*double confidence = pm.get_confidence_gpu();
	cout << confidence << endl;*/
	

	/*Rect bb = pm.get_best_buddies_gpu();
	Mat confidence_map = pm.get_confidence_map();
	Mat canvas = tar.clone();
	rectangle(canvas, bb, Scalar(0, 0, 255), 3);*/

	/*Rect srcrect(sx, sy, sw, sh);
	rectangle(src, srcrect, Scalar(255, 0, 0), 3);
	imshow("src", src);
	waitKey(0);*/

	/*imshow("canvas", canvas);
	waitKey(0);*/

	int64 t0 = cv::getTickCount();
    Point2i cnt = pm.get_best_buddies_gpu();
	Mat confidence_map = pm.get_confidence_map();
	cout << "get distance takes: " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << " ms." << endl;

    cout << cnt << endl;
    Rect rect(cnt.x - tw / 2, cnt.y - th / 2, tw, th);
	rectangle(tar, rect, Scalar(0, 0, 255), 3);
	imshow("target", tar);
    imshow("confidence", confidence_map);
	waitKey(0);
	return 0;
}