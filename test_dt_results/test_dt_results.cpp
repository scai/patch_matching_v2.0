#include "opencv2/opencv.hpp"
#include "dt_result.h"
#include "utils/maths.h"
#include "utils/drawing.h"
#include "patchMatching/patch_matching.h"

using namespace std;
using namespace cv;
using namespace statsmaster;

const string INPUT_ROOT = "D:\\Playground\\HorseRacing";
const string IMAGE_ROOT = INPUT_ROOT + "\\frames";
const string DETECTINO_ROOT = "E:\\HorseRacingData\\Train_Iteration\\jockeycap-3chn-pn\\iteration_1\\06May-race01-de-0.5p0.5n-64-5views-8games";

const vector<string> VIDEOS_RAW = { "06May-race01-de", "10Jun-race01", "10May-race06", "12Sep-race01", "15Oct-race01", "16May-race01-de", "18Jun-race01", "26Apr-race01" };
const string IMAGE_EXT = ".jpg";

void make_dt_list(const vector<vector<Rect>> & dt_rects, const vector<vector<float>> & dt_scores, vector<Vec2i> & all_indexs, vector<float> & all_scores);

int main(int argn, char **argv)
{
    // get the video name to process
    if (argn < 2) {
        printf("usage: .exe [Video Name]");
        exit(1);
    }

    string VIDEO = argv[1];

    // get the detection results
    vector<string> img_strs;
    vector<vector<Rect>> dt_rects;
    vector<vector<float>> dt_scores;
    vector<vector<int>> dt_labels;

    // parsing detection results
    string dt_fn = DETECTINO_ROOT + "\\" + VIDEO + ".txt";
    read_result_file(dt_fn, img_strs, dt_rects, dt_scores);

    vector<Vec2i> all_indexs;
    vector<float> all_scores;
    make_dt_list(dt_rects, dt_scores, all_indexs, all_scores);

    vector<int> idxs;
    sortIndex(all_scores, idxs, false);

    PatchMatching pm;
    //pm.set_src(src);
    //pm.set_tar(tar);
    //pm.set_src_patch(sx, sy, sw, sh);
    //pm.set_tar_patch(tx, ty, tw, th);
    //Rect bb = pm.get_best_buddies_gpu();
    //Mat confidence_map = pm.get_confidence_map();

    Mat img, cvs, img0, patch, conf;
    Rect rect, rect0;
    for (int ii = 0; ii < 200; ii++) {
        int i = idxs[ii];
        int n = all_indexs[i][0], k = all_indexs[i][1];
        printf("index: %d, %d, score: %f\n", all_indexs[i][0], all_indexs[i][1], all_scores[i]);

        if (n < 10 || n > img_strs.size() - 10)
            continue;

        img = imread(IMAGE_ROOT + "\\" + img_strs[n] + ".jpg");
        rect = dt_rects[n][k];
        cvs = img.clone();
        drawRects(dt_rects[n], cvs);
        imshow("img", cvs);

        pm.set_src(img);
        pm.set_src_patch(rect.x, rect.y, rect.width, rect.height);

        for (int j = -1; j <= 1; j++) {
            if (j == 0)
                continue;
            printf("checking %d\n", j);

            img0 = imread(IMAGE_ROOT + "\\" + img_strs[n + j] + ".jpg");
            rect0 = Rect(rect.x - 50, rect.y - 50, rect.width + 100, rect.height + 100);
            pm.set_tar(img0.clone());
            pm.get_best_buddies_gpu();
            conf = pm.get_confidence_map();
            imshow("conf " + to_string(j), conf * 5);

            drawRects(dt_rects[n + j], img0);
            patch = img0(rect0);
            imshow("patch " + to_string(j), patch);

            
        }
        
        char c = waitKey(0);
        if (c == 27)
            break;
    }
}

void make_dt_list(const vector<vector<Rect>> & dt_rects, const vector<vector<float>> & dt_scores, vector<Vec2i> & all_indexs, vector<float> & all_scores)
{
    for (int i = 0; i < dt_rects.size(); i++) {
        for (int j = 0; j < dt_rects[i].size(); j++) {
            all_indexs.push_back(Vec2i(i, j));
            all_scores.push_back(dt_scores[i][j]);
        }
    }
}




