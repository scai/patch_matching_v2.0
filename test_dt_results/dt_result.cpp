#include "dt_result.h"
#include <fstream>

using namespace std;
using namespace cv;

void read_result_file(string result_fn, vector<string> & str_set, vector<vector<Rect>> & bboxes, vector<vector<float>> & scores)
{
    ifstream fs;
    fs.open(result_fn.c_str());

    string line, str;
    vector<Rect> the_rects;
    Rect rect;
    vector<float> the_scores;
    float score;

    while (getline(fs, line)) {
        stringstream stream(line);
        stream >> str;
        stream >> rect.x >> rect.y >> rect.width >> rect.height;
        stream >> score;

        if (str_set.empty()) {
            str_set.push_back(str);
            the_rects.push_back(rect);
            the_scores.push_back(score);
        } else {
            if (str == str_set.back()) {
                the_rects.push_back(rect);
                the_scores.push_back(score);
            } else {
                str_set.push_back(str);
                bboxes.push_back(the_rects);
                scores.push_back(the_scores);
                the_rects.clear();
                the_scores.clear();
            }
        }

    }

    bboxes.push_back(the_rects);
    scores.push_back(the_scores);
}

void read_relevant_gt(string gt_root, vector<string> & str_set, vector<vector<Point2f>> & gt_cnts)
{
    string line;
    float x, y;
    for (int i = 0; i < str_set.size(); i++) {
        string gt_file = gt_root + "\\" + str_set[i] + ".csv";
        ifstream fs;
        fs.open(gt_file.c_str());

        vector<Point2f> cnts;
        while (getline(fs, line)) {
            replace(line.begin(), line.end(), ',', ' ');
            stringstream stream(line);
            stream >> x >> y;
            cnts.push_back(Point2f(x, y));
        }
        gt_cnts.push_back(cnts);

        fs.close();
    }
}

