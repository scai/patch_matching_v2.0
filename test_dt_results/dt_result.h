#include "opencv2/opencv.hpp"

using std::string;
using std::vector;

void read_result_file(string result_fn, vector<string> & str_set, vector<vector<cv::Rect>> & bboxes, vector<vector<float>> & scores);
void read_relevant_gt(string gt_root, vector<string> & str_set, vector<vector<cv::Point2f>> & gt_cnts);


