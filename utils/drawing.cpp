/*
* Copyright (c) Statsmaster Ltd, All Rights Reserved
*
* This is the utility for draw point, line, arc, text, etc. on an image.
*/

#include "drawing.h"

using namespace std;
using namespace cv;

Scalar randomColor(RNG & rng)
{
    return CV_RGB(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
}

void getColors(vector<Scalar> & colors)
{
    colors.clear();
    colors.push_back(CV_RGB(0, 0, 255));
    colors.push_back(CV_RGB(0, 255, 255));
    colors.push_back(CV_RGB(255, 0, 0));
    colors.push_back(CV_RGB(255, 0, 255));
    colors.push_back(CV_RGB(0, 255, 0));
}

//////////////////////////////////////////////////////////////////////////
// Draw Points
//////////////////////////////////////////////////////////////////////////

template <typename T>
void drawPoint(const Point_<T> & pnt, Mat & image, Scalar color, int radius, int thick, int iType)
{
    // for certain type of points
    if (iType == 0)
    {
        color = CV_RGB(0, 0, 255);
        radius = 2;
        thick = 2;
    } else if (iType == 1) {
        color = CV_RGB(100, 255, 255);
        radius = 4;
        thick = 2;
    } else if (iType == 2) {
        color = CV_RGB(255, 0, 0);
        radius = 6;
        thick = 2;
    } else if (iType == 3) {
        color = CV_RGB(255, 0, 255);
        radius = 8;
        thick = 2;
    }

    // for random color
    if (color(0) == -1)
        color = randomColor();

    circle(image, pnt, radius, color, thick);
}
template void drawPoint(const Point_<int> & pnt, Mat & image, Scalar color, int radius, int thick, int iType);
template void drawPoint(const Point_<float> & pnt, Mat & image, Scalar color, int radius, int thick, int iType);

template <typename T>
void drawPoints(const vector<T> & pts, Mat & image, Scalar color, int radius, int thick, vector<int> & iTypes)
{
    Scalar c = color;
    RNG rng = RNG(0x003FFFFF);
    for (size_t i = 0; i < pts.size(); i++)
    {
        if (color[0] == -1)
            c = randomColor(rng);

        if (iTypes.empty())
            drawPoint(Point2f(pts[i]), image, c, radius, thick);
        else
            drawPoint(Point2f(pts[i]), image, c, radius, thick, iTypes[i]);
    }
}
template void drawPoints(const vector<Point> & pts, Mat & image, Scalar color, int radius, int thick, vector<int> & iTypes);
template void drawPoints(const vector<Point2f> & pts, Mat & image, Scalar color, int radius, int thick, vector<int> & iTypes);

template <typename T>
void drawPoints(const vector<T> & pts, const Point2f & shift, Mat & image, Scalar color, int radius, int thick, vector<int> & iTypes)
{
    Scalar c = color;
    RNG rng = RNG(0x003FFFFF);
    for (size_t i = 0; i < pts.size(); i++)
    {
        if (color[0] == -1)
            c = randomColor(rng);

        if (iTypes.empty())
            drawPoint(Point2f(pts[i]) + shift, image, c, radius, thick);
        else
            drawPoint(Point2f(pts[i]) + shift, image, c, radius, thick, iTypes[i]);
    }
}
template void drawPoints(const vector<Point> & pts, const Point2f & ptshift, Mat & image, Scalar color, int radius, int thick, vector<int> & iTypes);
template void drawPoints(const vector<Point2f> & pts, const Point2f & ptshift, Mat & image, Scalar color, int radius, int thick, vector<int> & iTypes);

template <typename T>
void drawNPoints(const Point_<T> * pts, const Point2f & shift, Mat & image, int radius, int thick, int N)
{
    if (N >= 1)
        drawPoint(Point2f(pts[0]) + shift, image, CV_RGB(0, 0, 255), radius, thick);
    if (N >= 2)
        drawPoint(Point2f(pts[1]) + shift, image, CV_RGB(0, 255, 255), radius, thick);
    if (N >= 3)
        drawPoint(Point2f(pts[2]) + shift, image, CV_RGB(255, 0, 0), radius, thick);
    if (N >= 4)
        drawPoint(Point2f(pts[3]) + shift, image, CV_RGB(255, 0, 255), radius, thick);
    if (N >= 5)
        drawPoint(Point2f(pts[4]) + shift, image, CV_RGB(0, 255, 0), radius, thick);
    if (N >= 6)
        drawPoint(Point2f(pts[5]) + shift, image, CV_RGB(255, 255, 0), radius, thick);
    if (N > 6)
    {
        RNG rng = RNG(0x002FFFFF);
        for (size_t k = 6; k < N; k++)
            drawPoint(Point2f(pts[k]) + shift, image, randomColor(rng), radius, thick);
    }
}
template void drawNPoints(const Point * pts, const Point2f & shift, Mat & image, int radius, int thick, int N);
template void drawNPoints(const Point2f * pts, const Point2f & shift, Mat & image, int radius, int thick, int N);

template <typename T>
void drawNPoints(vector<Point_<T>> & pts, const Point2f & shift, Mat & image, int radius, int thick)
{
    drawPoint(Point2f(*(pts.end() - 4)) + shift, image, CV_RGB(0, 0, 255), radius, thick);
    drawPoint(Point2f(*(pts.end() - 3)) + shift, image, CV_RGB(0, 255, 255), radius, thick);
    drawPoint(Point2f(*(pts.end() - 2)) + shift, image, CV_RGB(255, 0, 0), radius, thick);
    drawPoint(Point2f(*(pts.end() - 1)) + shift, image, CV_RGB(255, 0, 255), radius, thick);
}
template void drawNPoints(vector<Point> & pts, const Point2f & shift, Mat & image, int radius, int thick);
template void drawNPoints(vector<Point2f> & pts, const Point2f & shift, Mat & image, int radius, int thick);

template <typename T>
void drawPointsPos(const vector<T> & pts, const Point2f & shift, Mat & image, Scalar color)
{
    for (size_t i = 0; i < pts.size(); i++)
    {
        drawText(i, image, Point2f(pts[i]) + shift, color);
        //drawText((int)pts[i].x, image, pts[i] + Point2f(-20, 20) + shift, color);
        //drawText((int)pts[i].y, image, pts[i] + Point2f(+10, 20) + shift, color);
    }
}
template void drawPointsPos(const vector<Point> & pts, const Point2f & shift, Mat & image, Scalar color);
template void drawPointsPos(const vector<Point2f> & pts, const Point2f & shift, Mat & image, Scalar color);

// draw two pairs of points on 2 images with matched color
template <typename T>
void drawPointPairs(const vector<Point_<T>> & pts0, Mat & image0, const vector<Point_<T>> & pts1, Mat & image1, int radius, int thick)
{
    for (size_t i = 0; i < pts0.size(); i++)
    {
        Scalar color = randomColor();
        drawPoint(pts0[i], image0, color, radius, thick);
        drawPoint(pts1[i], image1, color, radius, thick);
    }
}
template void drawPointPairs(const vector<Point> & pts0, Mat & image0, const vector<Point> & pts1, Mat & image1, int radius, int thick);
template void drawPointPairs(const vector<Point2f> & pts0, Mat & image0, const vector<Point2f> & pts1, Mat & image1, int radius, int thick);

// draw indexes on image with given corresponding positions
template <typename T0, typename T1>
void drawPointIndexes(const vector<T0> & idx, const vector<Point_<T1>> & pts, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck)
{
    for (size_t i = 0; i < idx.size(); i++)
    {
        drawText(idx[i], image, pts[i], colorTxt, colorBck, thickTxt, thickBck);
    }
}
template void drawPointIndexes(const vector<int> & idx, const vector<Point> & pts, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const vector<int> & idx, const vector<Point2f> & pts, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const vector<float> & idx, const vector<Point> & pts, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const vector<float> & idx, const vector<Point2f> & pts, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);

template <typename T0, typename T1>
void drawPointIndexes(const vector<T0> & idx, const vector<Point_<T1>> & pts, const Point2f & ptShift, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck)
{
    for (size_t i = 0; i < idx.size(); i++)
    {
        drawText(idx[i], image, Point2f(pts[i]) + ptShift, colorTxt, colorBck, thickTxt, thickBck);
    }
}
template void drawPointIndexes(const vector<int> & idx, const vector<Point> & pts, const Point2f & ptShift, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const vector<int> & idx, const vector<Point2f> & pts, const Point2f & ptShift, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const vector<float> & idx, const vector<Point> & pts, const Point2f & ptShift, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(const vector<float> & idx, const vector<Point2f> & pts, const Point2f & ptShift, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);

template <typename T>
void drawPointIndexes(int N, const vector<Point_<T>> & pts, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck)
{
    for (size_t i = 0; i < N; i++)
    {
        drawText(i, image, pts[i], colorTxt, colorBck, thickTxt, thickBck);
    }
}
template void drawPointIndexes(int N, const vector<Point> & pts, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(int N, const vector<Point2f> & pts, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);

template <typename T>
void drawPointIndexes(int N, const vector<Point_<T>> & pts, const Point2f & ptShift, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck)
{
    for (size_t i = 0; i < N; i++)
    {
        drawText(i, image, Point2f(pts[i]) + ptShift, colorTxt, colorBck, thickTxt, thickBck);
    }
}
template void drawPointIndexes(int N, const vector<Point> & pts, const Point2f & ptShift, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);
template void drawPointIndexes(int N, const vector<Point2f> & pts, const Point2f & ptShift, Mat & image, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck);

//////////////////////////////////////////////////////////////////////////
// Draw Lines
//////////////////////////////////////////////////////////////////////////

void drawLine(const Vec2f & line, Mat & image, Scalar color, int width)
{
    float rho = line[0], theta = line[1];
    Point pt1, pt2;
    double a = cos(theta), b = sin(theta);
    double x0 = a*rho, y0 = b*rho;
    pt1.x = cvRound(x0 + 1000 * (-b));
    pt1.y = cvRound(y0 + 1000 * (a));
    pt2.x = cvRound(x0 - 1000 * (-b));
    pt2.y = cvRound(y0 - 1000 * (a));
    cv::line(image, pt1, pt2, color, width, CV_AA);
}

void drawLine(const Vec4i & line, Mat & image, Scalar color, int width)
{
    cv::line(image, Point(line[0], line[1]), Point(line[2], line[3]), color, width, CV_AA);
}

void drawLine(const cv::Point2f & p1, const cv::Point2f & p2, cv::Mat & image, Scalar color, int width)
{
	cv::line(image, p1, p2, color, width, CV_AA);
}

void drawArrow(const Vec4i & line, Mat & image, Scalar color, int width)
{
    //cv::line(image, Point(line[0], line[1]), Point(line[2], line[3]), color, width, CV_AA);//line src.x src.y dst.x dst.y
    Point2f p, q;
    p.x = (int)line[0];
    p.y = (int)line[1];
    q.x = (int)line[2];
    q.y = (int)line[3];

    double angle = atan2((double)p.y - q.y, (double)p.x - q.x);
    double hypotenuse = sqrt((p.y - q.y)*(p.y - q.y) + (p.x - q.x)*(p.x - q.x));
    q.x = (int)(p.x - 3 * hypotenuse * cos(angle));
    q.y = (int)(p.y - 3 * hypotenuse * sin(angle));
    cv::line(image, p, q, color, width, CV_AA, 0);

    p.x = (int)(q.x + 9 * cos(angle + CV_PI / 4));
    p.y = (int)(q.y + 9 * sin(angle + CV_PI / 4));
    cv::line(image, p, q, color, width, CV_AA, 0);
    p.x = (int)(q.x + 9 * cos(angle - CV_PI / 4));
    p.y = (int)(q.y + 9 * sin(angle - CV_PI / 4));
    cv::line(image, p, q, color, width, CV_AA, 0);


}

void drawLine(const Vec4i & line, const Point2f & shift, Mat & image, Scalar color, int width)
{
    Vec4i lineShift(line);
    lineShift[0] += shift.x;
    lineShift[1] += shift.y;
    lineShift[2] += shift.x;
    lineShift[3] += shift.y;
    drawLine(lineShift, image, color, width);
}

void drawLine(const vector<Point> & line, cv::Mat & image, Scalar color, int width)
{
    cv::line(image, line.front(), line.back(), color, width, CV_AA);
}

void drawLines(const vector<Vec2f> & lines, Mat & image, Scalar color, int width, bool drawLabel)
{
    if (lines.empty())
        return;

    for (size_t i = 0; i < lines.size(); i++)
    {
        drawLine(lines[i], image, color, width);

        if (drawLabel)
        {
            char num[100] = "";
            sprintf(num, "Line %d", i);
            float rho = lines[i][0], theta = lines[i][1];
            double a = cos(theta), b = sin(theta);
            int x = image.cols / 3, y;
            if (b>FLT_EPSILON)
                y = (rho - x * a) / b;
            x = min(max(x, image.cols - 50), 10);
            y = min(max(y, image.rows - 20), 20);
            drawText(num, image, Point(x, y), color);
        }
    }
}

void drawLines(const vector<Vec4i> & lines, Mat & image, Scalar color, int width, bool drawLabel, const vector<int> linesId)
{
    if (lines.empty())
        return;

    Scalar c = color;
    RNG rng = RNG(0x00FFFAFF);
    for (size_t i = 0; i < lines.size(); i++)
    {
        if (color[0] == -1)
            c = randomColor(rng);
        drawLine(lines[i], image, c, width);

        if (drawLabel)
        {
            char num[100];
            if (linesId.empty())
            {
                sprintf_s(num, "Line %d", i);
            } else {
                if (linesId[i] == 255)
                    sprintf_s(num, "Other Lines");
                else if (linesId[i] >= 0)
                    sprintf_s(num, "H Line %d", linesId[i]);
                else
                    sprintf_s(num, "V Line %d", - linesId[i] - 1);
            }
            int x = (lines[i][0] + lines[i][2]) / 2, y = (lines[i][1] + lines[i][3]) / 2;
            x = max(min(x, image.cols - 80), 10);
            y = max(min(y, image.rows - 20), 20);
            drawText(num, image, Point(x, y), c);
        }
    }
}

void drawIndexedLines(const vector<Vec4i> & lines, const vector<int> lineIdx, const Point2f shift, Mat & image, Scalar color, int width, bool drawLabel)
{
    vector<Vec4i> mLines;
    for (size_t i = 0; i < lineIdx.size(); i++)
    {
        Vec4i mLine(lines[lineIdx[i]]);
        mLine[0] += shift.x; mLine[2] += shift.x;
        mLine[1] += shift.y; mLine[3] += shift.y;
        mLines.push_back(mLine);
    }

    drawLines(mLines, image, color, width, drawLabel);
}

void draw4Lines(const vector<Vec4i> & vLines, const vector<Vec4i> & hLines, const int index[], Mat & image, int width)
{
    if (vLines.empty() || hLines.empty())
        return;

    drawLine(vLines[index[0]], image, CV_RGB(0, 0, 255), width);
    drawLine(vLines[index[1]], image, CV_RGB(0, 255, 255), width);
    drawLine(hLines[index[2]], image, CV_RGB(255, 0, 0), width);
    drawLine(hLines[index[3]], image, CV_RGB(255, 0, 255), width);
}

void drawLineGroups(const vector<vector<Vec4i>> & groupLines, Mat & image, int width)
{
    vector<Scalar> colors;
    getColors(colors);
    for (size_t i = 0; i < groupLines.size(); i++)
        drawLines(groupLines[i], image, colors[i % colors.size()], width);
}

void drawIntersections(const Point2f * pts, int sz, Mat & image, bool label, Scalar color, int diameter)
{
    assert(!image.empty());
    for (int i = 0; i < sz; i++)
    {
        Scalar Color = randomColor();
        circle(image, pts[i], diameter, color, -1);
        char num[100] = "";
        sprintf(num, "P%d", i);
        putText(image, num, pts[i], 1, 1, Color);
    }
}

void drawIntersections(const vector<Point2f> & pts, Mat & image, bool label, Scalar color, int diameter)
{
    if (!pts.empty())
        drawIntersections(&pts[0], (int)pts.size(), image, label, color, diameter);
}

//////////////////////////////////////////////////////////////////////////
// Draw Arcs
//////////////////////////////////////////////////////////////////////////

template <typename T>
void drawArc(const vector<Point_<T>> & contour, Mat & image, Scalar color, int thick, bool bDrawDot)
{
    size_t length = contour.size();

    if (color[0] == -1)
        color = randomColor();

    // draw the starting point
    if (length > 0 && bDrawDot)
        circle(image, contour[0], 3, color);

    if (length > 1)
    {
        // draw the ending point and intermediate points
        if (bDrawDot)
        {
            for (int i = 0; i < length - 1; i++)
                circle(image, contour[i + 1], 1, CV_RGB(255, 255, 255), -1);
            circle(image, contour[length - 1], 3, color);
        }

        for (int i = 0; i < length - 1; i++)
            line(image, contour[i], contour[i + 1], color, thick);
    }
}
template void drawArc(const vector<Point> & contour, Mat & image, Scalar color, int thick, bool bDrawDot);
template void drawArc(const vector<Point2f> & contour, Mat & image, Scalar color, int thick, bool bDrawDot);

void drawArc(const Vec4i & contour, Mat & image, Scalar color, int thick, bool bDrawInfo, bool bDrawDot)
{
    vector<Point> line;
    line.push_back(Point(contour[0], contour[1]));
    line.push_back(Point(contour[2], contour[3]));
    drawArc(line, image, color, thick, bDrawDot);
}

template <typename T>
void drawArcs(const vector<vector<Point_<T>> > & contours, Mat & image, Scalar color, int thick, bool bDrawInfo, bool bDrawDot)
{
    Scalar c = color;
    RNG rng = RNG(0x001FFFFF);
    bool change = true;
    for (size_t i = 0; i < contours.size(); i++)
    {
        if (color[0] == -1)
            c = randomColor(rng);
        drawArc(contours[i], image, c, thick, bDrawDot);
        if (bDrawInfo)
            drawText(i, image, (contours[i].front() + contours[i].back()) * 0.5);
    }
}
template void drawArcs(const vector<vector<Point>> & contours, Mat & image, Scalar color, int thick, bool bDrawInfo, bool bDrawDot);
template void drawArcs(const vector<vector<Point2f>> & contours, Mat & image, Scalar color, int thick, bool bDrawInfo, bool bDrawDot);

//////////////////////////////////////////////////////////////////////////
// Draw Ellipses
//////////////////////////////////////////////////////////////////////////

void drawEllipse(const RotatedRect & ellispe, Mat & cdst, Scalar color, int thick)
{
    float h = ellispe.size.height;
    float w = ellispe.size.width;
    if (h >0 && w > 0)
    {
        float ratio = MAX(h, w) / MIN(h, w);
        //if (ratio<5)
        {
            if (color[0] == -1)
                ellipse(cdst, ellispe, randomColor(), thick);
            else
                ellipse(cdst, ellispe, color, thick);
        }
    }
}

void drawEllipse(const RotatedRect & ellipse, const Point2f & shift, Mat & image, Scalar color, int thick)
{
    RotatedRect ell_shift(ellipse);
    ell_shift.center.x += shift.x;
    ell_shift.center.y += shift.y;
    drawEllipse(ell_shift, image, color, thick);
}

void drawEllipses(const vector<RotatedRect> & ellipses, Mat & image, Scalar color, int thick)
{
    Scalar c = color;
    RNG rng = RNG(0x000FFFFF);
    for (size_t i = 0; i < ellipses.size(); i++)
    {
        if (color[0] == -1)
            c = randomColor(rng);
        drawEllipse(ellipses[i], image, c, thick);
    }
}

void drawEllipseInfo(const RotatedRect & ellipse, Mat & image, Scalar color1, Scalar color2, int thick)
{
    if (color1[0] == -1)
        color1 = CV_RGB(255, 0, 0);
    if (color2[0] == -1)
        color2 = CV_RGB(0, 255, 0);

    Point2f p0(ellipse.center);
    drawPoint(p0, image, color1, 5, -1);
    float alpha = ellipse.angle / 180 * CV_PI;
    Point2f v0(cos(alpha), sin(alpha));
    Point2f v1(cos(alpha + CV_PI / 2), sin(alpha + CV_PI / 2));
    drawPoint(p0 + v0 * ellipse.size.width * 0.5, image, color1, 5, -1);
    drawPoint(p0 - v0 * ellipse.size.width * 0.5, image, color1, 5, -1);
    drawPoint(p0 + v1 * ellipse.size.height * 0.5, image, color2, 5, -1);
    drawPoint(p0 - v1 * ellipse.size.height * 0.5, image, color2, 5, -1);
}

void drawEllipsesInfo(const vector<RotatedRect> & ellipses, Mat & image, Scalar color1, Scalar color2, int thick)
{
    for (size_t i = 0; i < ellipses.size(); i++)
        drawEllipseInfo(ellipses[i], image, color1, color2, thick);
}

//////////////////////////////////////////////////////////////////////////
// Draw a Box
//////////////////////////////////////////////////////////////////////////

template <typename T>
void drawRect(const Rect_<T> & rect, Mat & image, Scalar color, int thick)
{
    if (color[0] == -1)
        color = randomColor();
    line(image, Point_<T>(rect.x, rect.y), Point_<T>(rect.x + rect.width, rect.y), color, thick);
    line(image, Point_<T>(rect.x, rect.y), Point_<T>(rect.x, rect.y + rect.height), color, thick);
    line(image, Point_<T>(rect.x + rect.width, rect.y + rect.height), Point_<T>(rect.x + rect.width, rect.y), color, thick);
    line(image, Point_<T>(rect.x + rect.width, rect.y + rect.height), Point_<T>(rect.x, rect.y + rect.height), color, thick);
}
template void drawRect(const Rect & rect, Mat & image, Scalar color, int thick);
template void drawRect(const Rect_<float> & rect, Mat & image, Scalar color, int thick);

template <typename T>
void drawRect(const Vec<T, 4> & rect, Mat & image, Scalar color, int thick)
{
    Rect rect_(rect[0], rect[1], rect[2] - rect[0] + 1, rect[3] - rect[1] + 1);
    drawRect(rect_, image, color, thick);
}
template void drawRect(const Vec4i & rect, Mat & image, Scalar color, int thick);
template void drawRect(const Vec4f & rect, Mat & image, Scalar color, int thick);

template <typename T>
void drawRects(const vector<Rect_<T>> & rects, Mat & image, Scalar color, int thick)
{
    for (int i = 0; i < rects.size(); i++) {
        drawRect(rects[i], image, color, thick);
    }
}
template void drawRects(const vector<Rect> & rects, Mat & image, Scalar color, int thick);
template void drawRects(const vector<Rect_<float>> & rects, Mat & image, Scalar color, int thick);

// Draw a rect surround the pixels on the image
void drawSurroundingRect(const Mat pxlImg, Mat canvas, Scalar color, int thick)
{
	int x0 = pxlImg.cols - 1, y0 = pxlImg.rows - 1, x1 = 0, y1 = 0;
	for (int x = 0; x < pxlImg.cols; x++) {
		for (int y = 0; y < pxlImg.rows; y++) {
			if (pxlImg.ptr<uchar>(y)[x] != 0) {
				if (x < x0)
					x0 = x;
				if (x > x1)
					x1 = x;
				if (y < y0)
					y0 = y;
				if (y > y1)
					y1 = y;
			}
		}
	}

	if (color[0] == -1)
		color = randomColor();

	if (x0 < x1 && y0 < y1)
		drawRect(Vec4i(x0, y0, x1, y1), canvas, color, thick);
}

//////////////////////////////////////////////////////////////////////////
// draw time

void drawTime(string name, float t, string unit, Mat & canvas, const Point2f & p, Scalar colorTxt, Scalar colorBck, int thickTxt, int thickBck)
{
	t = floor(t * 1000) / 1000;

	ostringstream ss;
	ss << name << ": " << t << " " << unit;
	putText(canvas, ss.str(), p, 1, 0.8, colorBck, thickBck);
	putText(canvas, ss.str(), p, 1, 0.8, colorTxt, thickTxt);
}

//////////////////////////////////////////////////////////////////////////
// Draw Camera Informations
//////////////////////////////////////////////////////////////////////////

void drawCameraInfo(Mat & cvsModel, double alpha, double dCamCnt[], double theta, double beta, double gamma)
{
    drawText("Alpha_U, Alpha_V = ", cvsModel, Point(10, 40));
    drawText(alpha, cvsModel, Point(160, 40));
    drawText("Camera Center X, Y, Z = ", cvsModel, Point(10, 60));
    drawText(dCamCnt[0], cvsModel, Point(40, 80));
    drawText(dCamCnt[1], cvsModel, Point(110, 80));
    drawText(dCamCnt[2], cvsModel, Point(170, 80));
    drawText("Theta, Beta, Gamma = ", cvsModel, Point(10, 100));
    drawText(theta, cvsModel, Point(40, 120));
    drawText(beta, cvsModel, Point(130, 120));
    drawText(gamma, cvsModel, Point(220, 120));
}

void drawCameraInfo(Mat & cvsModel, double alpha, Point3f dCamCnt3f, double theta, double beta, double gamma)
{
    double dCamCnt[3];
    dCamCnt[0] = dCamCnt3f.x;
    dCamCnt[1] = dCamCnt3f.y;
    dCamCnt[2] = dCamCnt3f.z;
    drawCameraInfo(cvsModel, alpha, dCamCnt, theta, beta, gamma);
}

//////////////////////////////////////////////////////////////////////////
// Display homography matrix

void drawHomography(Mat & cvsModel, const Mat & H, string txt, const Point2f & ptStart)
{
    // convert the Mat
    double h[9];
    Mat _H(3, 3, CV_64F, h);
    H.convertTo(_H, _H.type());

    if (abs(h[8] - 1) < 1e-5)
        h[8] = 1;

    // leave 2 digits
    for (int i = 0; i < 9; i++)
        h[i] = floor(h[i] * 1000) / 1000;

    // display
    drawText(txt, cvsModel, ptStart - Point2f(0, 20));
    double c = 50, r = 20;
    double shtX[] = { 0, c, c * 2, 0, c, c * 2, 0, c, c * 2 };
    double shtY[] = { 0, 0, 0, r, r, r, r * 2, r * 2, r * 2 };
    for (int i = 0; i < 9; i++)
        drawText(h[i], cvsModel, ptStart + Point2f(shtX[i], shtY[i]));
}