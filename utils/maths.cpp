/*
* Copyright (c) Statsmaster Ltd, All Rights Reserved
*
* This is the math related functions used for the pitch processor.
*/

#include "maths.h"

using namespace std;
using namespace cv;

namespace statsmaster
{
    // Sigmoid function
    float sigmoid(float x, float a, float b)
    {
        float y = 1.0f / (1 + exp(-(a * x + b)));
        return y;
    }

    void sigmoid(const Mat & x, Mat & y, float a, float b)
    {
        Mat tmp;
        exp(-(x * a + b), tmp);
        y = 1.0 / (1 + tmp);
    }

    // Solve quadratic function
    bool findQuadraticRoots(double *coefficients, double *roots)
    {
        double a = coefficients[0];
        double b = coefficients[1];
        double c = coefficients[2];
        bool sign = false;
        double det = b * b - 4 * a * c;
        if (det >= 0){
            roots[0] = (-b + sqrt(det)) / (2 * a);
            roots[1] = (-b - sqrt(det)) / (2 * a);
            sign = 1;
        }

        return sign;
    }

    // Truncate digits more than n
    float truncateDigits(float s, int n) {
        s = floor(s * pow(10, n)) / pow(10, n);
        return s;
    }
}