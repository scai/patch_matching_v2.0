/*
* Copyright (c) Statsmaster Ltd, all rights reserved
*
* This is the utility for draw point, line, arc, text, etc. on an image.
*
*/

#ifndef _DRAWING_H
#define _DRAWING_H

#include "opencv2/opencv.hpp"

using cv::Mat;
using cv::Scalar;
using cv::Vec;
using cv::Vec4i;
using cv::Vec2f;
using cv::Point_;
using cv::Point;
using cv::Point2f;
using cv::Point3f;
using cv::Rect_;
using cv::RotatedRect;
using cv::RNG;
using std::vector;

Scalar randomColor(RNG & rng = RNG(0xFFFFFFFF));
void getColors(vector<Scalar> & colors);

// Draw points
template <typename T>
void drawPoint(const Point_<T> & pnt, Mat & image, Scalar color = Scalar::all(-1), int radius = 3, int thick = 1, int iType = -1);

template <typename T>
void drawPoints(const vector<T> & pts, Mat & image, Scalar color = Scalar::all(-1), int radius = 3, int thick = 1, vector<int> & iTypes = vector<int>());

template <typename T>
void drawPoints(const vector<T> & pts, const Point2f & shift, Mat & image, Scalar color = Scalar::all(-1), int radius = 3, int thick = 1, vector<int> & iTypes = vector<int>());

template <typename T>
void drawNPoints(const Point_<T> * pts, const Point2f & shift, Mat & image, int radius = 3, int thick = 1, int N = 4);

template <typename T>
void drawNPoints(vector<Point_<T>> & pts, const Point2f & shift, Mat & image, int radius = 3, int thick = 1);

template <typename T>
void drawPointsPos(const vector<T> & pts, const Point2f & shift, Mat & image, Scalar color = Scalar::all(-1));

template <typename T>
void drawPointPairs(const vector<Point_<T>> & pts0, Mat & image0, const vector<Point_<T>> & pts1, Mat & image1, int radius = 3, int thick = 1);

template <typename T0, typename T1>
void drawPointIndexes(const vector<T0> & idx, const vector<Point_<T1>> & pts, Mat & image, Scalar colorTxt = Scalar::all(255), Scalar colorBck = Scalar::all(0), int thickTxt = 1, int thickBck = 3);
template <typename T0, typename T1>
void drawPointIndexes(const vector<T0> & idx, const vector<Point_<T1>> & pts, const Point2f & ptShift, Mat & image, Scalar colorTxt = Scalar::all(255), Scalar colorBck = Scalar::all(0), int thickTxt = 1, int thickBck = 3);
template <typename T>
void drawPointIndexes(int N, const vector<Point_<T>> & pts, Mat & image, Scalar colorTxt = Scalar::all(255), Scalar colorBck = Scalar::all(0), int thickTxt = 1, int thickBck = 3);
template <typename T>
void drawPointIndexes(int N, const vector<Point_<T>> & pts, const Point2f & ptShift, Mat & image, Scalar colorTxt = Scalar::all(255), Scalar colorBck = Scalar::all(0), int thickTxt = 1, int thickBck = 3);

// draw lines
void drawLine(const cv::Vec2f & line, cv::Mat & image, Scalar color = CV_RGB(255, 0, 0), int width = 1);
void drawLine(const cv::Vec4i & line, cv::Mat & image, Scalar color = CV_RGB(255, 0, 0), int width = 1);
void drawLine(const cv::Point2f & p1, const cv::Point2f & p2, cv::Mat & image, Scalar color = CV_RGB(255, 0, 0), int width = 1);
void drawLine(const Vec4i & line, const Point2f & shift, Mat & image, Scalar color, int width);
void drawLine(const vector<Point> & line, cv::Mat & image, Scalar color = CV_RGB(255, 0, 0), int width = 1);
void drawLines(const vector<Vec2f> & lines, Mat & image, Scalar color = CV_RGB(255, 0, 0), int width = 1, bool drawLabel = false);
void drawLines(const vector<Vec4i> & lines, Mat & image, Scalar color = Scalar::all(-1), int width = 1, bool drawLabel = false, const vector<int> linesId = vector<int>());

//draw arrows
void drawArrow(const cv::Vec4i & line, cv::Mat & image, Scalar color = CV_RGB(255, 0, 0), int width = 1);

void drawIndexedLines(const vector<Vec4i> & lines, const vector<int> lineIdx, const Point2f shift, Mat & image, Scalar color = Scalar::all(-1), int width = 1, bool drawLabel = false);

void draw4Lines(const vector<Vec4i> & vLines, const vector<Vec4i> & hLines, const int index[], Mat & image, int width = 1);

void drawLineGroups(const vector<vector<Vec4i>> & lines, Mat & image, int width = 1);

void drawIntersections(const vector<Point2f> & pts, Mat & image, bool label = true, Scalar color = CV_RGB(0, 255, 0), int diameter = 3);
void drawIntersections(const Point2f * pts, int sz, Mat & image, bool label = true, Scalar color = CV_RGB(0, 255, 0), int diameter = 3);

// draw arcs
template <typename T>
void drawArc(const vector<Point_<T>> & contour, Mat & image, Scalar color = cvScalar(0, 0, 255), int thick = 1, bool bDrawDot = true);
void drawArc(const Vec4i & contour, Mat & image, Scalar color = Scalar::all(-1), int thick = 1, bool bDrawDot = true);

template <typename T>
void drawArcs(const vector<vector<Point_<T>> > & contours, Mat & image, Scalar color = Scalar::all(-1), int thick = 1, bool bDrawInfo = false, bool bDrawDot = true);

// draw ellipses
void drawEllipse(const RotatedRect & ellispe, Mat & image, Scalar color = Scalar::all(-1), int thick = 1);
void drawEllipse(const RotatedRect & ellispe, const Point2f & shift, Mat & image, Scalar color = Scalar::all(-1), int thick = 1);
void drawEllipses(const vector<RotatedRect> & ellispes, Mat & image, Scalar color = Scalar::all(-1), int thick = 1);
void drawEllipseInfo(const RotatedRect & ellipse, Mat & image, Scalar color1 = Scalar::all(-1), Scalar color2 = Scalar::all(-1), int thick = 1);
void drawEllipsesInfo(const vector<RotatedRect> & ellipses, Mat & image, Scalar color1 = Scalar::all(-1), Scalar color2 = Scalar::all(-1), int thick = 1);

// Draw a box
template <typename T>
void drawRect(const Rect_<T> & rect, Mat & image, Scalar color = Scalar::all(-1), int thick = 1);
template <typename T>
void drawRect(const Vec<T, 4> & rect, Mat & image, Scalar color = Scalar::all(-1), int thick = 1);

template <typename T>
void drawRects(const vector<Rect_<T>> & rect, Mat & image, Scalar color = Scalar::all(-1), int thick = 1);

// Draw a rect surround the pixels on the image
void drawSurroundingRect(const Mat pxlImg, Mat canvas, Scalar color = Scalar(-1), int thick = 1);

// draw text
template <typename T0, typename T1>
void drawText(T0 txt, Mat & canvas, const Point_<T1> & p, Scalar colorTxt = Scalar::all(255), Scalar colorBck = Scalar::all(0), int thickTxt = 1, int thickBck = 3)
{
    std::ostringstream ss;
    ss << txt;
    putText(canvas, ss.str(), p, 1, 0.8, colorBck, thickBck);
    putText(canvas, ss.str(), p, 1, 0.8, colorTxt, thickTxt);
}

// draw time
void drawTime(std::string name, float t, std::string unit, Mat & canvas, const Point2f & p, Scalar colorTxt = Scalar::all(255), Scalar colorBck = Scalar::all(0), int thickTxt = 1, int thickBck = 3);

// Display camera info
void drawCameraInfo(Mat & cvsModel, double alpha, double dCamCnt[], double theta, double beta, double gamma);
void drawCameraInfo(Mat & cvsModel, double alpha, Point3f dCamCnt3f, double theta, double beta, double gamma);

// Display homography matrix
void drawHomography(Mat & cvsModel, const Mat & H, std::string txt = "Homography:", const Point2f & ptStart = Point2f(10, 520));

#endif
