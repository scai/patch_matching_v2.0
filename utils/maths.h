/*
 * Copyright (c) Statsmaster Ltd, All Rights Reserved
 * 
 * This is the math related functions used for the pitch processor.
 */

#ifndef _MATHS_H
#define _MATHS_H

#include "opencv2/opencv.hpp"

namespace statsmaster
{
    using cv::Mat;

    // Get (row, col) in a matrix
    #define Mx(h, i, j) h.at<double>(i-1, j-1)

    // Swap two simple number;
    template< typename T>
    void swapAB( T * a, T * b )
    {
        T tmp = *a;
        *a = *b;
        *b = tmp;
    }

    // Sigmoid function
    float sigmoid(float x, float a = 1.0f, float b = 0.0f);
    void sigmoid(const Mat & x, Mat & y, float a = 1.0f, float b = 0.0f);

    // Sort and get the sorted index
    template<typename T>
    struct less_cmp {
        less_cmp(const T varr) : arr(varr) {}
        inline bool operator()(const size_t a, const size_t b) const
        {
            return arr[a] < arr[b];
        }
        const T arr;
    };

    template<typename T>
    struct larger_cmp {
        larger_cmp(const T varr) : arr(varr) {}
        inline bool operator()(const size_t a, const size_t b) const
        {
            return arr[a] > arr[b];
        }
        const T arr;
    };

    template<typename T>
    void sortIndex(const std::vector<T> & vals, std::vector<int> & idx, bool bAscending = true)
    {
        if (vals.empty())
            return;

        if (idx.empty() || idx.size() != vals.size())
        {
            idx.clear();
            for (int i = 0; i < (int)vals.size(); idx.push_back(i++));
        }

        if (bAscending)
            sort(idx.begin(), idx.end(), less_cmp<const std::vector<T> &>(vals));
        else
            sort(idx.begin(), idx.end(), larger_cmp<const std::vector<T> &>(vals));
    }

    // Solve quadratic function
    bool findQuadraticRoots(double *coefficients, double *roots);

    // Truncate digits more than n
    float truncateDigits(float s, int n = 2);
}

#endif
