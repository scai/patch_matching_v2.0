#include "opencv2/opencv.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include "opencv2/imgcodecs.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/ml.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <opencv2/features2d/features2d.hpp>
/////has cuda
#include "opencv2/core/cuda.hpp"
#include "opencv2/cudacodec.hpp"
#include "opencv2/cudaarithm.hpp"

#include "opencv2/core/cuda.hpp"
#include "opencv2/cudacodec.hpp"

#define COMPUTE_GPU

class PatchMatching{
public:
	PatchMatching();
	
	~PatchMatching();

	void init();
	void set_src(const cv::Mat& src);
	void set_tar(const cv::Mat& tar);
	void set_src_patch(int x = 0, int y = 0, int width = 0, int height = 0);
	void set_tar_patch(int x = 0, int y = 0, int width = 0, int height = 0);
	

	double get_confidence();
	cv::Rect get_best_buddies();
	cv::Mat get_confidence_map();

	int get_bb_gpu();
    cv::Point2i get_best_buddies_gpu();
    cv::Point2i get_best_buddies_gpu(const cv::Mat & tar_patch);

private:
	cv::Mat _src_img;
	cv::Mat _src_img_r, _src_img_g, _src_img_b;
	cv::cuda::GpuMat _src_img_gpu;
	cv::cuda::GpuMat _src_img_gpu_r, _src_img_gpu_g, _src_img_gpu_b;
	cv::Mat _tar_img;
	cv::cuda::GpuMat _tar_img_gpu;

	cv::Mat _src_patch;
	cv::cuda::GpuMat _src_patch_gpu;
	cv::Mat _tar_patch;
	cv::cuda::GpuMat _tar_patch_gpu;

    cv::cuda::GpuMat _d_rgb_buffer, _d_xy_buffer, _src_bbs, _tar_bbs, _d_confidence;
    cv::Mat _confidence_map, _confidence_map_shift;
    cv::Point2i _bb_cnt;

    float _gamma = 2.f;
	int _sample_step;
	int _patch_sample;
    std::vector<float> _filter;
    cv::cuda::GpuMat _d_filter;
};


#pragma comment(lib, "patchMatching.lib")