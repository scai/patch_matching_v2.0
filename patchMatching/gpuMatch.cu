﻿#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <cuda_device_runtime_api.h>
#include <device_launch_parameters.h>
#include "opencv2/opencv.hpp"
#include "opencv2/cudaarithm.hpp"
#include "utils.h"
#include "patch_matching.h"
#include <fstream>

using namespace cv;
using namespace std;
using namespace cv::cuda;

__global__ void getDistance(const PtrStepSz<uchar> src_img_b, const PtrStepSz<uchar> src_img_g, const PtrStepSz<uchar> src_img_r, const PtrStepSz<uchar> tar_patch_b, const PtrStepSz<uchar> tar_patch_g, const PtrStepSz<uchar> tar_patch_r, 
							int src_rows, int src_cols, int tar_patch_rows, int tar_patch_cols, float* d_distance,int sampleStep){
	const int c = blockIdx.x * blockDim.x + threadIdx.x;
	const int r = blockIdx.y * blockDim.y + threadIdx.y;


	if (c < 0 || c>= src_cols || r < 0 || r >= src_rows) return;
	if (c % sampleStep != 0 || r % sampleStep != 0) return;
	

	for (int i = 0; i < tar_patch_rows/sampleStep; i++){
		for (int j = 0; j < tar_patch_cols/sampleStep; j++){
			float sum = 0.0;
			for (int m = 0; m < sampleStep; m++){
				for (int n = 0; n < sampleStep; n++){
					int v0 = src_img_b(r, c) - tar_patch_b(i*sampleStep + m, j*sampleStep + n);
					int v1 = src_img_g(r, c) - tar_patch_g(i*sampleStep + m, j*sampleStep + n);
					int v2 = src_img_r(r, c) - tar_patch_r(i*sampleStep + m, j*sampleStep + n);
					sum += sqrtf(v0*v0 + v1*v1 + v2*v2) / sqrtf(3.0);
				}
			}
			*(d_distance + (r * src_cols + c) * tar_patch_rows* tar_patch_cols + i * sampleStep * tar_patch_cols + j * sampleStep) = sum / (sampleStep*sampleStep);
		}
	}

}

__global__ void getDistance(const PtrStepSz<float3> src_img, const PtrStepSz<float3> tar_pth, int src_rows, int src_cols, int tar_rows, int tar_cols, int pz, PtrStepSz<float> d_rgb, PtrStepSz<float> filter)
{
    const int c = blockIdx.x * blockDim.x + threadIdx.x;
    const int r = blockIdx.y * blockDim.y + threadIdx.y;

    if (c < 0 || c >= src_cols || r < 0 || r >= src_rows) return;
    const int r_img = r * pz, c_img = c * pz;
    const int tar_sz = tar_cols * tar_rows;
    const int d_step = d_rgb.step / sizeof(float);

    for (int i = 0; i < tar_rows; i++) {
        for (int j = 0; j < tar_cols; j++) {
            float *dst = d_rgb + r * d_step + c * tar_sz + i * tar_cols + j;
            const int r_pth = i * pz, c_pth = j * pz;

            float sum = 0.0;
            for (int m = 0; m < pz; m++) {
                for (int n = 0; n < pz; n++) {
                    float v0 = (src_img(r_img + m, c_img + n).x - tar_pth(r_pth + m, c_pth + n).x) * filter[m, n];
                    float v1 = (src_img(r_img + m, c_img + n).y - tar_pth(r_pth + m, c_pth + n).y) * filter[m, n];
                    float v2 = (src_img(r_img + m, c_img + n).z - tar_pth(r_pth + m, c_pth + n).z) * filter[m, n];
                    sum += v0*v0 + v1*v1 + v2*v2;
                }
            }
            *dst = sum;
        }
    }

}

__global__ void getBestBuddies(const PtrStepSz<uchar> src_img, const PtrStepSz<uchar> tar_patch, int src_rows, int src_cols, int tar_patch_rows, int tar_patch_cols, int sampleStep, float* d_distance, PtrStepSz<int> best_buddies, int*  best_buddies_reverse){
	const int c = blockIdx.x * blockDim.x + threadIdx.x;
	const int r = blockIdx.y * blockDim.y + threadIdx.y;
	if (c < 0 || c >= src_cols || r < 0 || r >= src_rows) return;
	//if (c % sampleStep != 0 || r % sampleStep != 0) return;

	float minDistance = 100;
	int index = -1;
	for (int i = 0; i < tar_patch_rows/sampleStep; i++){
		for (int j = 0; j < tar_patch_cols/sampleStep; j++){
			float temp = *(d_distance + (r * src_cols + c) * tar_patch_rows * tar_patch_cols + i * sampleStep * tar_patch_cols + j * sampleStep);
			if (temp < minDistance){
				minDistance = temp;
				index = i * tar_patch_cols * sampleStep  + j * sampleStep;
			}
		}
	}
	best_buddies(r, c) = index;//index;
	

	if (r + tar_patch_rows > src_rows || c + tar_patch_cols > src_cols) return;
	for (int m = 0; m < tar_patch_rows/sampleStep; m++){
		for (int n = 0; n < tar_patch_cols/ sampleStep; n++){
			float minDistance = 100;
			int index = -1;
			for (int i = 0; i < tar_patch_rows/sampleStep; i++){
				for (int j = 0; j < tar_patch_cols/sampleStep; j++){
					
					float temp = *(d_distance + ((r + i * sampleStep) * src_cols + c + j * sampleStep) * tar_patch_rows * tar_patch_cols + m * sampleStep * tar_patch_cols + n * sampleStep);
					if (temp < minDistance){
						minDistance = temp;
						index = (i* sampleStep) * tar_patch_cols + j * sampleStep;
					}
				}
			}
			*(best_buddies_reverse +  (r * src_cols + c) * tar_patch_rows * tar_patch_cols + m * sampleStep * tar_patch_cols + n * sampleStep)  = index;
		}
	}
}

__global__ void getBestBuddies(PtrStepSz<float> d_distance, int src_rows, int src_cols, int tar_rows, int tar_cols, int pz, PtrStepSz<int> src_bbs, PtrStepSz<int> tar_bbs)
{
    const int c = blockIdx.x * blockDim.x + threadIdx.x;
    const int r = blockIdx.y * blockDim.y + threadIdx.y;

    // get every src patch's best buddies in tar template
    if (c < 0 || c >= src_cols || r < 0 || r >= src_rows) return;
    const int d_step = d_distance.step / sizeof(float);
    const int tar_sz = tar_cols * tar_rows;

    float minDistance = 100;
    int index = -1;
    for (int i = 0; i < tar_rows; i++){
        for (int j = 0; j < tar_cols; j++){
            float dist = *(d_distance + r * d_step + c * tar_sz + i * tar_cols + j);

            if (dist < minDistance){
                minDistance = dist;
                index = i * tar_cols + j;
            }
        }
    }
    src_bbs(r, c) = index;

    // get every tar patch's best buddies in every slide window of src
    if (r + tar_rows > src_rows || c + tar_cols > src_cols) return;
    const int tar_step = tar_bbs.step / sizeof(int);

    // (m, n) indicates a tar patch
    for (int m = 0; m < tar_rows; m++) {
        for (int n = 0; n < tar_cols; n++) {
            // (i, j) indicates the window starting from (r, c)
            float minDistance = 100;
            int index = -1;
            for (int i = 0; i < tar_rows; i++) {
                for (int j = 0; j < tar_cols; j++) {
                    float dist = *(d_distance + (r + i) * d_step + (c + j) * tar_sz + m * tar_cols + n);

                    if (dist < minDistance){
                        minDistance = dist;
                        index = i * tar_cols + j;
                    }
                }
            }
            *(tar_bbs + r * tar_step + c * tar_sz + m * tar_cols + n) = index;
        }
    }
}

__global__ void getConfidence(const PtrStepSz<int> best_buddies, int* best_buddies_reverse, int src_rows, int src_cols, int tar_patch_rows, int tar_patch_cols, PtrStepSz<int> confidence_mat, int sampleStep){
	const int c = blockIdx.x * blockDim.x + threadIdx.x;
	const int r = blockIdx.y * blockDim.y + threadIdx.y;
	if (c < 0 || c + tar_patch_cols> src_cols || r < 0 || r + tar_patch_rows > src_rows) return;
	if (c % sampleStep != 0 || r % sampleStep != 0) return;
	
	for (int i = 0; i < tar_patch_rows/sampleStep; i++){
		for (int j = 0; j < tar_patch_cols/sampleStep; j++){
			int index = best_buddies(r + i * sampleStep, c + j * sampleStep);
			int temp = *(best_buddies_reverse + (r * src_cols + c) * tar_patch_rows * tar_patch_cols + index);
			int srow = temp / tar_patch_cols;
			int scol = temp % tar_patch_cols;
			if (srow == i * sampleStep  && scol == j * sampleStep )
				//atomicAdd(&confidence_mat(r, c), 1);
				confidence_mat(r, c)++;
		}
	}

}

__global__ void getConfidence(const PtrStepSz<int> src_bbs, const PtrStepSz<int> tar_bbs, int src_rows, int src_cols, int tar_rows, int tar_cols, int pz, PtrStepSz<float> confidence)
{
    const int c = blockIdx.x * blockDim.x + threadIdx.x;
    const int r = blockIdx.y * blockDim.y + threadIdx.y;
    if (c + tar_cols > src_cols || r + tar_rows > src_rows) return;
    const int tar_step = tar_bbs.step / sizeof(int);
    const int tar_sz = tar_cols * tar_rows;

    int matched = 0;
    for (int i = 0; i < tar_rows; i++) {
        for (int j = 0; j < tar_cols; j++) {
            // the src patch's best buddy on tar
            int idx_2_tar = src_bbs(r + i, c + j);
            // the chosen tar patch's best buddy
            int m = idx_2_tar / tar_cols, n = idx_2_tar - m * tar_cols;
            int idx_2_src = *(tar_bbs + r * tar_step + c * tar_sz + m * tar_cols + n);

            if (idx_2_src == i * tar_cols + j)
                matched++;
        }
    }

    confidence(r, c) = (float)matched / tar_sz;
}

__global__ void getDistanceXY(PtrStepSz<float> distance_xy, int tar_patch_rows, int tar_patch_cols){
	const int c = blockIdx.x * blockDim.x + threadIdx.x;
	const int r = blockIdx.y * blockDim.y + threadIdx.y;
	int N = tar_patch_rows * tar_patch_cols;
	if (c < 0 || c >= N || r < 0 || r >= N) return;
	
	int r1 = r / tar_patch_cols;
	int c1 = r % tar_patch_cols;
	int r2 = c / tar_patch_cols;
	int c2 = c % tar_patch_cols;

	distance_xy(r, c) = sqrtf((r2 - r1) * (r2 - r1) + (c2 - c1) * (c2 - c1)) / sqrtf(2);
}

int PatchMatching::get_bb_gpu(){
	const dim3 blockSize(16, 16);
	dim3 gridSize((_src_img.cols + blockSize.x - 1) / blockSize.x, (_src_img.rows + blockSize.y - 1) / blockSize.y);

	int N = _tar_patch_gpu.rows * _tar_patch_gpu.cols;
	dim3 gridSize2((N + blockSize.x -1) / blockSize.x, (N + blockSize.y - 1) / blockSize.y);
	GpuMat distance_xy(N, N, CV_32FC1);
	getDistanceXY<<<gridSize2, blockSize>>>(distance_xy, _tar_patch_gpu.rows, _tar_patch_gpu.cols);

	
	float* d_distance;
	if (sizeof(d_distance) / sizeof(float) != _src_img.rows * _src_img.cols * _tar_patch_gpu.rows *  _tar_patch_gpu.cols)
		checkCudaErrors(cudaMalloc(&d_distance, sizeof(float)* _src_img.rows * _src_img.cols * _tar_patch_gpu.rows *  _tar_patch_gpu.cols ));
	
	GpuMat splitted_t_gpu[3];
	split(_tar_patch_gpu, splitted_t_gpu);
	
	getDistance << <gridSize, blockSize >> >(_src_img_gpu_b, _src_img_gpu_g, _src_img_gpu_r, splitted_t_gpu[0], splitted_t_gpu[1], splitted_t_gpu[2], _src_img.rows, _src_img.cols, _tar_patch_gpu.rows, _tar_patch_gpu.cols, d_distance, _patch_sample);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());



	/*float* h_distance = new float[_src_img.rows * _src_img.cols * _tar_patch_gpu.rows *  _tar_patch_gpu.cols];
	checkCudaErrors(cudaMemcpy(h_distance, d_distance, sizeof(float)* _src_img.rows * _src_img.cols * _tar_patch_gpu.rows *  _tar_patch_gpu.cols, cudaMemcpyDeviceToHost));

	for (int i = 0; i <_src_img.rows * _src_img.cols * _tar_patch_gpu.rows * _tar_patch_gpu.cols; i++){
		cout << temp[i] - h_distance[i] << endl;
	}*/


	/*Mat dd(_src_img.rows, _src_img.cols, CV_64FC1);
	for (int i = 0; i < _src_img.rows; i++){
		for (int j = 0; j < _src_img.cols; j++){
			dd.at<float>(i, j) = h_distance[(i*_src_img.cols + j) * _tar_patch_gpu.rows  * _tar_patch_gpu.cols];
		}
	}

	cout << _tar_patch.rows << " * " << _tar_patch.cols << " = " << _tar_patch.rows * _tar_patch.cols << endl;*/
	
	GpuMat best_buddies(_src_img.rows, _src_img.cols, CV_32SC1);
	int* best_buddies_reverse;
	if (sizeof(best_buddies_reverse) / sizeof(int) != _src_img.rows * _src_img.cols * _tar_patch_gpu.rows *  _tar_patch_gpu.cols)
		checkCudaErrors(cudaMalloc(&best_buddies_reverse, sizeof(int)* _src_img.rows * _src_img.cols * _tar_patch_gpu.rows *  _tar_patch_gpu.cols));
	getBestBuddies << <gridSize, blockSize >> >(_src_img_gpu, _tar_img_gpu, _src_img.rows, _src_img.cols, _tar_patch_gpu.rows, _tar_patch_gpu.cols, _patch_sample,d_distance, best_buddies, best_buddies_reverse);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());

	/*Mat best_buddies_cpu;
	best_buddies.download(best_buddies_cpu);*/

/*
	int* bb_reverse = new int[_src_img.rows * _src_img.cols * _tar_patch_gpu.rows *  _tar_patch_gpu.cols];
	checkCudaErrors(cudaMemcpy(bb_reverse, best_buddies_reverse, sizeof(int)* _src_img.rows * _src_img.cols * _tar_patch_gpu.rows *  _tar_patch_gpu.cols, cudaMemcpyDeviceToHost));

	Mat bb(_src_img.rows, _src_img.cols, CV_32SC1);
	for (int i = 0; i < _src_img.rows; i++){
		for (int j = 0; j < _src_img.cols; j++){
			bb.at<int>(i, j) = bb_reverse[(i*_src_img.cols + j) * _tar_patch_gpu.rows  * _tar_patch_gpu.cols];
		}
	}*/

	GpuMat confidence_mat(_src_img.rows, _src_img.cols, CV_32SC1);
	getConfidence << <gridSize, blockSize >> >(best_buddies, best_buddies_reverse, _src_img.rows, _src_img.cols, _tar_patch_gpu.rows, _tar_patch_gpu.cols, confidence_mat,_patch_sample);
	cudaDeviceSynchronize();
	checkCudaErrors(cudaGetLastError());

	Mat h_confidence_mat;
	confidence_mat.download(h_confidence_mat);
	
	int index = 0;
	int max_confidence = 0;
	Mat confidence(h_confidence_mat.rows/_patch_sample, h_confidence_mat.cols/_patch_sample, h_confidence_mat.type());
	for (int i = 0; i < h_confidence_mat.rows/_patch_sample; i++){
		for (int j = 0; j < h_confidence_mat.cols/_patch_sample; j++){
			if (h_confidence_mat.at<int>(i*_patch_sample, j*_patch_sample) > max_confidence){
				max_confidence = h_confidence_mat.at<int>(i*_patch_sample, j*_patch_sample);
				index = i * _patch_sample * _src_img.cols + j * _patch_sample;

			}
			confidence.at<int>(i, j) = h_confidence_mat.at<int>(i*_patch_sample, j * _patch_sample);
		}
	}
	confidence.convertTo(confidence, CV_32FC1);
	//normalize(confidence, confidence);

	for (int i = 0; i < h_confidence_mat.rows / _patch_sample; i++){
		for (int j = 0; j < h_confidence_mat.cols / _patch_sample; j++){
			confidence.at<float>(i, j) = confidence.at<float>(i,j) / max_confidence;
		}
	}
	resize(confidence, confidence, Size(confidence.cols * _patch_sample, confidence.rows * _patch_sample), INTER_NEAREST);
	/*imshow("confidence", confidence * 255);
	waitKey(0);*/
	_confidence_map = Mat::zeros(confidence.rows, confidence.cols, CV_32FC1);
	for (int i = 0; i < confidence.rows - _tar_patch.rows / 2; i++){
		for (int j = 0; j < confidence.cols - _tar_patch.cols / 2; j++){
			_confidence_map.at<float>(i + _tar_patch.rows/2, j + _tar_patch.cols/2) = confidence.at<float>(i, j);
		}
	}

	cout << index << endl;
	cout << max_confidence << endl;
	/*delete[] d_distance;
	delete[] best_buddies_reverse;*/
	return index;
}

Point2i PatchMatching::get_best_buddies_gpu()
{
    if (_src_img.type() == CV_8UC3) {
        _src_img.convertTo(_src_img, CV_32FC3, 1. / 255);
        _src_img_gpu.upload(_src_img);
    }

    if (_tar_patch.type() == CV_8UC3) {
        _tar_patch.convertTo(_tar_patch, CV_32FC3, 1. / 255);
        _tar_patch_gpu.upload(_tar_patch);
    }

    int tar_rows = _tar_patch_gpu.rows / _patch_sample, tar_cols = _tar_patch_gpu.cols / _patch_sample;
    int src_rows = _src_img_gpu.rows / _patch_sample, src_cols = _src_img_gpu.cols / _patch_sample;
    int tar_sz = tar_cols * tar_rows;

    const dim3 blockSize(16, 16);
    dim3 gridSize((src_cols + blockSize.x - 1) / blockSize.x, (src_rows + blockSize.y - 1) / blockSize.y);

    //if (_d_xy_buffer.cols != tar_sz || _d_xy_buffer.rows != tar_sz) {
    //    _d_xy_buffer = GpuMat(tar_sz, tar_sz, CV_32F);
    //}
    //dim3 gridSizeT((tar_sz + blockSize.x - 1) / blockSize.x, (tar_sz + blockSize.y - 1) / blockSize.y);
    //getDistanceXY << <gridSizeT, blockSize >> >(_d_xy_buffer, tar_rows, tar_cols);

    if (_d_rgb_buffer.cols != src_cols || _d_rgb_buffer.rows != src_rows) {
        _d_rgb_buffer = GpuMat(src_rows, src_cols, CV_32FC(tar_sz));
    }

    int64 t0 = getTickCount();
    getDistance << <gridSize, blockSize >> >(_src_img_gpu, _tar_patch_gpu, src_rows, src_cols, tar_rows, tar_cols, _patch_sample, _d_rgb_buffer, _d_filter);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());
    printf("get distance using: %f ms\n", (getTickCount() - t0) / getTickFrequency() * 1000);

    if (_src_bbs.cols != src_cols || _src_bbs.rows != src_rows) {
        _src_bbs = GpuMat(src_rows, src_cols, CV_32S);
    }
    if (_tar_bbs.cols != src_cols || _tar_bbs.rows != src_rows) {
        _tar_bbs = GpuMat(src_rows, src_cols, CV_32SC(tar_sz));
    }

    t0 = getTickCount();
    getBestBuddies << <gridSize, blockSize >> >(_d_rgb_buffer, src_rows, src_cols, tar_rows, tar_cols, _patch_sample, _src_bbs, _tar_bbs);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());
    printf("get best buddies: %f ms\n", (getTickCount() - t0) / getTickFrequency() * 1000);

    if (_d_confidence.cols != src_cols || _d_confidence.rows != src_rows) {
        _d_confidence = GpuMat(src_rows, src_cols, CV_32F);
    }

    t0 = getTickCount();
    getConfidence << <gridSize, blockSize >> >(_src_bbs, _tar_bbs, src_rows, src_cols, tar_rows, tar_cols, _patch_sample, _d_confidence);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());
    printf("get confidence: %f ms\n", (getTickCount() - t0) / getTickFrequency() * 1000);

    if (_confidence_map.cols != src_cols || _confidence_map.rows != src_rows)
        _confidence_map = Mat::zeros(src_rows, src_cols, CV_32F);
    else
        _confidence_map.setTo(0);

    _d_confidence.download(_confidence_map_shift);
    _confidence_map_shift(Rect(0, 0, src_cols - tar_cols, src_rows - tar_rows)).copyTo(
        _confidence_map(Rect(tar_cols / 2, tar_rows / 2, src_cols - tar_cols, src_rows - tar_rows)));
    resize(_confidence_map, _confidence_map, _src_img.size());

    // find the top location
    float max_score = 0;
    for (int x = 0; x < _confidence_map.cols; x++) {
        for (int y = 0; y < _confidence_map.rows; y++) {
            if (_confidence_map.ptr<float>(y)[x] > max_score) {
                max_score = _confidence_map.ptr<float>(y)[x];
                _bb_cnt.x = x;
                _bb_cnt.y = y;
            }
        }
    }

    return _bb_cnt;
}

Point2i PatchMatching::get_best_buddies_gpu(const Mat & tar_patch)
{
    _tar_patch = tar_patch.clone();
    return get_best_buddies_gpu();
}




