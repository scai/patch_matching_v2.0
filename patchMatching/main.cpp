﻿#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/cudaarithm.hpp"
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <vector>
#include "tool.h"
#include <unordered_set>
//#include "gpuMatch.cu"

using namespace cv;
using namespace std;
using namespace cv::cuda;

double matchGPU(const GpuMat& srcPatchGPU, const  GpuMat& tarPatchGPU, int numRows1, int numCols1, int numRows2, int numCols2);

Rect matchPatchGPU(const Mat& src, const Rect& srcRect, const Mat& target){
	Rect res;
	Mat srcPatch = src(srcRect);
	GpuMat srcPatchGPU;
	srcPatchGPU.upload(srcPatch);
	int numRows1 = srcPatch.rows;
	int numCols1 = srcPatch.cols;
	int N1 = numRows1 * numCols1;
	int lambda = 2;

	int sampleStep = 5;
	double maxConfidence = 0;
	int maxIndex = 0;
	
	int64 t0 = cv::getTickCount();
	for (int x = 0; x <= target.cols - srcRect.width; x += sampleStep){
		for (int y = 0; y <= target.rows - srcRect.height; y += sampleStep){
			//cout << x << ", " << y << "\t";
			int numRows2 = numRows1;
			int numCols2 = numCols1;
			int N2 = numRows2 * numCols2;
			Rect curRect = Rect(x, y, numCols2, numRows2);
			Mat tarPatch = target(curRect);
			GpuMat tarPatchGPU;
			tarPatchGPU.upload(tarPatch);
			
			double confidence = matchGPU(srcPatchGPU, tarPatchGPU, numRows1, numCols1, numRows2, numCols2);
			
			//cout << confidence << endl;
			if (confidence > maxConfidence){
				maxConfidence = confidence;
				res = curRect;
			}	
		}
	}
	cout << "takes: " << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 <<" ms. "<< endl;
	return res;
}

double  getConfidenceGPU(const Mat& src, const Mat& target){
	double confidence = 0;
	int numRows1 = src.rows;
	int numCols1 = src.cols;
	int numRows2 = target.rows;
	int numCols2 = target.cols;
	int N2 = numRows2 * numCols2;
	
	GpuMat srcGPU, targetGPU;
	srcGPU.upload(src);
	targetGPU.upload(target);

	confidence = matchGPU(srcGPU, targetGPU, numRows1, numCols1, numRows2, numCols2);
	return confidence;
}

Rect matchPatch(const Mat& src,const Rect& srcRect, const Mat& target){
	Rect res;
	Mat srcPatch = src(srcRect);
	Mat splitted_s[3];
	split(srcPatch, splitted_s);
	int numRows = srcPatch.rows;
	int numCols = srcPatch.cols;
	int N1 = numRows * numCols;
	int lambda = 2;

	int sampleStep = 5;
	double maxConfidence = 0.0;


	//ofstream out("result.txt");
//#pragma omp parallel for
	for (int x = 0; x <= target.cols - srcRect.width; x += sampleStep){
		for (int y = 0; y <= target.rows - srcRect.height; y += sampleStep){
			for (int w = numCols*0.8; w <= numCols*1.2; w++){
				for (int h = numRows * 0.8; h <= numRows*1.2; h++){

					Rect curRect = Rect(Point(x, y), Point(min(x + w, numCols-1), min(y + h, numRows-1)));
					Mat tarPatch = target(curRect);
					int numRowt = tarPatch.rows;
					int numColt = srcPatch.cols;
					int N2 = numRowt* numColt;


					Mat distance = Mat::zeros(N1, N2, CV_32FC1);
					Mat splitted_t[3];
					split(tarPatch, splitted_t);

					for (int i = 0; i < N1; i++){
						for (int j = 0; j < N2; j++){
							//cout << i << ", " << j << endl;
							int diffR = (int)splitted_s[0].at<uchar>(i / numCols, i % numCols) - splitted_t[0].at<uchar>(j / numColt, j % numColt);
							int diffG = (int)splitted_s[1].at<uchar>(i / numCols, i % numCols) - splitted_t[1].at<uchar>(j / numColt, j % numColt);
							int diffB = (int)splitted_s[2].at<uchar>(i / numCols, i % numCols) - splitted_t[2].at<uchar>(j / numColt, j % numColt);
							int distanceXY = (i / numCols - j / numColt) * (i / numCols - j / numColt) + (i % numCols - j % numColt) * (i % numCols - j % numColt);
							distance.at<float>(i, j) = lambda * sqrt(diffR * diffR + diffG * diffG + diffB * diffB) / (255 * sqrt(3));// +(float)sqrt(distanceXY) / sqrt(numRows*numRows + numCols * numCols);
						}
					}

					vector<int> array1(N1, 0);
					for (int i = 0; i < N1; i++){
						float dist = 0.1;
						int index = -1;
						for (int j = 0; j < N2; j++){
							if (distance.at<float>(i, j) < dist){
								dist = distance.at<float>(i, j);
								index = j;
							}
						}
						array1[i] = index;
					}

					vector<int> array2(N2, 0);
					for (int i = 0; i < N2; i++){
						float dist = 0.1;
						int index = -1;
						for (int j = 0; j < N1; j++){
							if (distance.at<float>(j, i) < dist){
								dist = distance.at<float>(j, i);
								index = j;
							}
						}
						array2[i] = index;
					}

					int count = 0;
					for (int i = 0; i < N2; i++){
						if (array2[i] != -1 && array1[array2[i]] == i)
							count++;
					}
					cout << "count: " << count << endl;
					int smaller = N1 < N2 ? N1 : N2;
					double confidence = (double)count / (double)smaller;
					//#pragma omp critical
					if (confidence > maxConfidence){
						maxConfidence = confidence;
						res = curRect;
					}
				}
			}
		}
	}
	//out.close();
	return res;
}

double  getConfidence(const Mat& src, const Rect& rect, const Mat& target, const Rect& rect2){
	Rect res;
	Mat srcPatch = src(rect);
	Mat splitted_s[3];
	split(srcPatch, splitted_s);
	int numRows = srcPatch.rows;
	int numCols = srcPatch.cols;
	int N1 = numRows * numCols;

	Mat tarPatch = target(rect2);
	int numRowt = tarPatch.rows;
	int numColt = srcPatch.cols;
	int N2 = numRowt* numColt;

	int lambda = 2;
			
			
			Mat distance = Mat::zeros(N1, N2, CV_32FC1);
			Mat splitted_t[3];
			split(tarPatch, splitted_t);
			for (int i = 0; i < N1; i++){
				for (int j = 0; j < N2; j++){
					//cout << i << ", " << j << endl;
					int diffR = (int)splitted_s[0].at<uchar>(i / numCols, i % numCols) - splitted_t[0].at<uchar>(j / numColt, j % numColt);
					int diffG = (int)splitted_s[1].at<uchar>(i / numCols, i % numCols) - splitted_t[1].at<uchar>(j / numColt, j % numColt);
					int diffB = (int)splitted_s[2].at<uchar>(i / numCols, i % numCols) - splitted_t[2].at<uchar>(j / numColt, j % numColt);
					int distanceXY = (i / numCols - j / numColt) * (i / numCols - j / numColt) + (i % numCols - j % numColt) * (i % numCols - j % numColt);
					distance.at<float>(i, j) = lambda * sqrt(diffR * diffR + diffG * diffG + diffB * diffB) / (255 * sqrt(3));// +(float)sqrt(distanceXY) / sqrt(numRows*numRows + numCols * numCols);
				}
			}

			vector<int> array1(N1, 0);
			for (int i = 0; i < N1; i++){
				float dist = 0.1;
				int index = -1;
				for (int j = 0; j < N2; j++){
					if (distance.at<float>(i, j) < dist){
						dist = distance.at<float>(i, j);
						index = j;
					}
				}
				array1[i] = index;
			}

			vector<int> array2(N2, 0);
			for (int i = 0; i < N2; i++){
				float dist = 0.1;
				int index = -1;
				for (int j = 0; j < N1; j++){
					if (distance.at<float>(j, i) < dist){
						dist = distance.at<float>(j, i);
						index = j;
					}
				}
				array2[i] = index;
			}

			int count = 0;
			for (int i = 0; i < N2; i++){
				if (array2[i] != -1 && array1[array2[i]] == i)
					count++;
			}
			cout << "count: " << count << "\t ";
			int smaller = N1 < N2 ? N1 : N2;
			double confidence = (double)count / (double)smaller;
			return confidence;
}

int main(int argc, char* argv[]){
	Mat src = imread(argv[1]);//"E:\\ImageParsingProject\\data\\HorseRace\\train\\00193.jpg"
	resize(src, src, Size(src.cols / 2, src.rows / 2));
	int srcx = atoi(argv[2])/2; //531 / 2;
	int srcy = atoi(argv[3])/2;  //491 / 2;
	/*int sheight = 41/2;
	int swidth = 33/2;*/
	int sheight = atoi(argv[4])/2;// 22;
	int swidth = atoi(argv[5])/2; 17;
	Rect rect;
	rect = Rect(srcx, srcy, swidth, sheight);
	/*rectangle(src, rect, Scalar(255, 0, 0), 3);
	imshow("source", src);
	waitKey(1);*/
	int operation = 4;
	if (operation == 1){
		vector<string> files = readDir("E:\\ImageParsingProject\\data\\HorseRace\\train\\");
		int i = 0;
		for (auto file : files){
			Mat target = imread(file);
			resize(target, target, Size(target.cols / 2, target.rows / 2));
			
			Mat roiImg;
			roiImg = src(rect);

			imshow("src", roiImg);
			///*waitKey(0);*/

			Rect res;
			int64 t0 = cv::getTickCount();
			res = matchPatch(src, rect, target);
			cout << (cv::getTickCount() - t0) / cv::getTickFrequency() * 1000 << " ms. " << endl;

			cout << res.tl().x << ", " << res.tl().y << endl;

			Mat resImage;
			/*resImage = target(res);
			imshow("res", resImage);*/
			rectangle(target, res.tl(), res.br(), Scalar(0, 0, 255), 2);
			imshow("target", target);

			waitKey(1);
			imwrite("result2/" + std::to_string((int)i) + ".jpg", target);
			i++;
		}
	} else if (operation ==2 ){
		Mat target = imread("E:\\ImageParsingProject\\data\\HorseRace\\train\\00209.jpg");

		resize(target, target, Size(target.cols / 2, target.rows / 2));
		int tlx = 256;//800
		int tly = 242; // 270
		int theight = 30;
		int twidth = 30;
	
		Rect rect2;
		rect2 = Rect(tlx, tly, twidth, theight);
		double  confidence = getConfidence(src, rect, target, rect2);
		cout <<"confidence: "<< confidence << endl;
		system("pause");
	} else if (operation == 3){
		Mat target = imread("E:\\ImageParsingProject\\data\\HorseRace\\train\\00209.jpg");
		resize(target, target, Size(target.cols / 2, target.rows / 2));
		Rect rectGPU = matchPatchGPU(src, rect, target);
		rectangle(target, rectGPU, Scalar(0, 0, 255), 3);
		cout << rectGPU.tl().x << "\t" << rectGPU.tl().y << endl;
		imshow("target", target);
		waitKey(0);
	}
	else {
		Mat roiImg;
		roiImg = src(rect);
		Mat target = imread(argv[6]);//"E:\\ImageParsingProject\\data\\HorseRace\\train\\00209.jpg"
		resize(target, target, Size(target.cols / 2, target.rows / 2));
		Rect rectTar(atoi(argv[7])/2, atoi(argv[8])/2, min(target.cols - atoi(argv[7]), atoi(argv[10]))/2, min(target.rows - atoi( argv[8]), atoi(argv[9]))/2);
		Mat roiTar;
		roiTar = target(rectTar);
		/*rectangle(target, rectTar, Scalar(0, 0, 255), 3);
		imshow("target", target);
		waitKey(1);*/

		/*cout << rect.tl().x << ", " << rect.tl().y <<", "<< rect.width<<", "<<rect.height<< endl;
		cout << rectTar.tl().x << ", " << rectTar.tl().y <<", "<<rectTar.width<<", "<<rectTar.height<< endl;*/

		double confidence = getConfidenceGPU(roiTar, roiImg);
		cout << confidence << endl;
		rectangle(src, rect, Scalar(255, 0, 0), 3);
		rectangle(target, rectTar, Scalar(0, 0, 255),3);
		imshow("src", src);
		imshow("target", target);
		waitKey(0);
		//system("pause");
	}
	
	return 0;
}