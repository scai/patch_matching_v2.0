#ifndef TOOL_H
#define TOOL_H
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <boost\filesystem.hpp>

struct EvalResult{
	int groundTruth;
	std::string route;
	int right;
	int wrong;
	float successRate;
	std::vector<int> predict;
	std::vector<cv::Mat> currentFeature;

};



std::vector<std::string> split(const std::string&, char);
void write_file(std::string filename, cv::Mat data);
cv::Mat read_file(std::string filename);
void debug(std::string msg, cv::Mat& mat);
std::string std::to_string(int i);
std::vector<std::string> readDir(std::string dir);

std::string base_path(std::string path);
std::string ExtractPath(std::string filename);
std::string ExtractFileExtention(std::string filename);
std::string ExtractFileName(std::string filename, bool withExtension = true);
bool fileExistCheck(const char* filename, std::ifstream& inputfile);


#endif